package at.htlwels.it.insy.main;


import static com.winklerlabs.ASCIIWriter.*;

// Dieses Project soll dazu dienen, die Grundfunktionalitäten von Jakarta Persistence zu demonstrieren.
// Es erhebt keinen Anspruch auf Vollständigkeit.
// Auf Englisch als Hauptsprache wurde nicht geachtet.
// Auf Exception-Handling wurde bewusst verzichtet, ist aber im Produktiveinsatz unabdingbar.

public class Main {

    private static final String PERSISTENCE_UNIT_NAME = "JP_SQLSERVER";

    public static void main(String[] args) throws Exception {

        printSignatureOne();
        System.out.println("**************************************************");
        System.out.println("***   Jakarta Persistence Demo                    ");
        System.out.println("**************************************************");

        at.htlwels.it.insy.model.a_singleobject.Test.persistSingleObject(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.b_classhierarchy.Test.persistClassHierarchy(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.c_onetotoone.Test.persistOneToOneRelationship(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.d_onetomany.Test.persistOneToManyRelationship(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.e_manytomany.Test.persistManyToManyRelationship(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.f_composition.Test.persistComposition(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.g_recursion.Test.persistRecursion(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.h_lifecycle.Test.showLifeCycle(PERSISTENCE_UNIT_NAME);
        at.htlwels.it.insy.model.i_queries.Test.showQueries(PERSISTENCE_UNIT_NAME);
    }
}
