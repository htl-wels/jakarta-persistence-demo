package at.htlwels.it.insy.model.c_onetotoone.bidirectional;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
public class Login {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String loginKennung;

    @OneToOne (mappedBy = "login")
    private Benutzer benutzer;

    public Login() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getLoginKennung() {
        return loginKennung;
    }

    public void setLoginKennung(String login) {
        this.loginKennung = login;
    }

    public Benutzer getBenutzer() {
        return benutzer;
    }

    public void setBenutzer(Benutzer benutzer) {
        this.benutzer = benutzer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Login)) return false;
        Login that = (Login) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getLoginKennung(), that.getLoginKennung());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getLoginKennung());
    }

    @Override
    public String toString() {
        return "LoginKonto{" +
                "oid=" + oid +
                ", login='" + loginKennung + '\'' +
                '}';
    }
}
