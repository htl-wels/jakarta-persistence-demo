package at.htlwels.it.insy.model.c_onetotoone.bidirectional;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
public class Benutzer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String vorname;
    private String familienname;

    // Das dazugehörige LoginKonto wird automatisch mitgeladen, aber nicht automatisch auch gespeichert)
    @OneToOne (fetch = FetchType.EAGER)
    @JoinColumn (name = "loginOID")
    private Login login;

    public Benutzer() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getFamilienname() {
        return familienname;
    }

    public void setFamilienname(String familienname) {
        this.familienname = familienname;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Benutzer)) return false;
        Benutzer benutzer = (Benutzer) o;
        return Objects.equals(getOid(), benutzer.getOid()) &&
                Objects.equals(getVorname(), benutzer.getVorname()) &&
                Objects.equals(getFamilienname(), benutzer.getFamilienname()) &&
                Objects.equals(getLogin(), benutzer.getLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getVorname(), getFamilienname(), getLogin());
    }

    @Override
    public String toString() {
        return "Benutzer{" +
                "oid=" + oid +
                ", vorname='" + vorname + '\'' +
                ", familienname='" + familienname + '\'' +
                ", loginKonto=" + login +
                '}';
    }
}
