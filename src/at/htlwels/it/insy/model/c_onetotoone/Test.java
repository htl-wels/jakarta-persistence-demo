package at.htlwels.it.insy.model.c_onetotoone;

import at.htlwels.it.insy.model.c_onetotoone.bidirectional.Benutzer;
import at.htlwels.it.insy.model.c_onetotoone.bidirectional.Login;
import at.htlwels.it.insy.model.c_onetotoone.unidirectional.Postanschrift;
import at.htlwels.it.insy.model.c_onetotoone.unidirectional.Teilnehmer;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

public class Test {

    public static void persistOneToOneRelationship(String PERSISTENCE_UNIT_NAME) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        // OneToOne - Unidirectional
        /*
        ----------------------                       -----------------------
        |                    |                    1  |                     |
        |    Teilnehmer      |---------------------->|    Postanschrift    |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */
        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist an Association - OneToOne - Unidirectional                  ");

        entityManager.getTransaction().begin();

        Teilnehmer teilnehmer = new Teilnehmer();
        teilnehmer.setNachname("Knackpostibischil");
        teilnehmer.setVorname("Hans");

        Postanschrift pa = new Postanschrift();
        pa.setStrasseMitHausnummer("Kracherlweg 8");
        pa.setPostleitzahl("4444");
        pa.setOrt("Felberhausen");

        teilnehmer.setPostanschrift(pa);
        entityManager.persist(teilnehmer);

        entityManager.getTransaction().commit();

        // OneToOne - Bidirectional
         /*
        ----------------------                       -----------------------
        |                    |  1                 1  |                     |
        |     Benutzer       |-----------------------|        Login        |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */

        System.out.println("***   Persist an Association - OneToOne - Bidirectional                  ");

        entityManager.getTransaction().begin();

        Benutzer benutzer = new Benutzer();
        benutzer.setFamilienname("Portisch");
        benutzer.setVorname("Hugo");

        Login login = new Login();
        login.setLoginKennung("hugoPort");
        login.setBenutzer(benutzer);
        benutzer.setLogin(login);

        entityManager.persist(benutzer);
        entityManager.persist(login);

        entityManager.getTransaction().commit();

        System.out.println("**************************************************");
        System.out.println("***   Validation - Read OneToOne Associations");
        System.out.println("**************************************************");

        // Read OnetoOne Unidirectional Association

        entityManager.getTransaction().begin();

        TypedQuery<Teilnehmer> queryT = entityManager.createQuery("select t from Teilnehmer t", Teilnehmer.class);
        Teilnehmer theTeilnehmer =  queryT.getSingleResult();
        System.out.printf("***   %s%n", theTeilnehmer.toString());
        System.out.printf("***   %s%n", theTeilnehmer.getPostanschrift().toString());

        entityManager.getTransaction().commit();

        // Read OnetoOne Bidirectional Association
        entityManager.getTransaction().begin();
        TypedQuery<Benutzer> queryB = entityManager.createQuery("select b from Benutzer b", Benutzer.class);
        Benutzer theBenutzer =  queryB.getSingleResult();
        System.out.printf("***   %s%n", theBenutzer.toString());
        System.out.printf("***   %s%n", theBenutzer.getLogin().toString());
        System.out.println("**************************************************");
        entityManager.getTransaction().commit();

        entityManager.close();
    }
}
