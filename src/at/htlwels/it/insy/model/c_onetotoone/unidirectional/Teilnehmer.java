package at.htlwels.it.insy.model.c_onetotoone.unidirectional;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
public class Teilnehmer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String vorname;
    private String nachname;

    // Durch cascade = CascadeType.All wird festgelegt, dass die Postanschrift automatisch mitgespeichert wird
    // Durch fetch = FetchType.EAGER wird festgelegt, wenn der Teilnehmer gelesen wird, dann wird automatisch auch die
    // Postanschrift mitgelesen.
    @OneToOne (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "postanschriftOID")
    private Postanschrift postanschrift;

    public Teilnehmer() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Postanschrift getPostanschrift() {
        return postanschrift;
    }

    public void setPostanschrift(Postanschrift postanschrift) {
        this.postanschrift = postanschrift;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teilnehmer)) return false;
        Teilnehmer that = (Teilnehmer) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getVorname(), that.getVorname()) &&
                Objects.equals(getNachname(), that.getNachname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getVorname(), getNachname());
    }

    @Override
    public String toString() {
        return "Teilnehmer{" +
                "oid=" + oid +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                '}';
    }
}
