package at.htlwels.it.insy.model.c_onetotoone.unidirectional;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.Objects;

@Entity
public class Postanschrift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String strasseMitHausnummer;
    private String ort;
    private String postleitzahl;

    public Postanschrift() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getStrasseMitHausnummer() {
        return strasseMitHausnummer;
    }

    public void setStrasseMitHausnummer(String strasseMitHausnummer) {
        this.strasseMitHausnummer = strasseMitHausnummer;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getPostleitzahl() {
        return postleitzahl;
    }

    public void setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Postanschrift)) return false;
        Postanschrift that = (Postanschrift) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getStrasseMitHausnummer(), that.getStrasseMitHausnummer()) &&
                Objects.equals(getOrt(), that.getOrt()) &&
                Objects.equals(getPostleitzahl(), that.getPostleitzahl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getStrasseMitHausnummer(), getOrt(), getPostleitzahl());
    }

    @Override
    public String toString() {
        return "Postanschrift{" +
                "oid=" + oid +
                ", strasseMitHausnummer='" + strasseMitHausnummer + '\'' +
                ", ort='" + ort + '\'' +
                ", postleitzahl='" + postleitzahl + '\'' +
                '}';
    }
}
