package at.htlwels.it.insy.model.a_singleobject;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.util.List;

public class Test {

    public static void persistSingleObject(String PERSISTENCE_UNIT_NAME) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        // Persistierung eines Objektes
        /*
        ----------------------
        |                    |
        |      Person        |
        |                    |
        ----------------------
         */

        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist an Object                   ");


        entityManager.getTransaction().begin();

        Person karlOtto = new Person();
        karlOtto.setFamilieNname("Otto");
        karlOtto.setVorName("Karl");
        karlOtto.setGeburtsOrt("Zeltweg");
        karlOtto.setOffice365Kennung("karl.otto@zeltweg.at");
        karlOtto.setSchnulNetzKennzeichen("KROT");

        entityManager.persist(karlOtto);

        entityManager.getTransaction().commit();

        System.out.println("**************************************************");
        System.out.println("***   Validation - Read persisted Objects");
        System.out.println("**************************************************");

        entityManager.getTransaction().begin();

        TypedQuery<Person> query = entityManager.createQuery("select p from Person p", Person.class);
        List<Person> personenList = query.getResultList();

        // This is pragmatic Code, not more.
        for (Person tempPerson : personenList) {
            System.out.println("***   " + tempPerson.toString());
        }
        System.out.println("**************************************************");
        entityManager.getTransaction().commit();

        entityManager.close();
    }
}
