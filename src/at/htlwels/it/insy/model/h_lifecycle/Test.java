package at.htlwels.it.insy.model.h_lifecycle;

import at.htlwels.it.insy.model.a_singleobject.Person;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

public class Test {

    public static void showLifeCycle(String PERSISTENCE_UNIT_NAME) {

        // In dieser Methode werden die wichtigesten CRUD-Vorgehensweisen dargestellt

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();


        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Speichern eines Objektes                  ");

        // Speichern eines Objektes
        Person jamesBond = new Person();
        jamesBond.setVorName("James");
        jamesBond.setFamilieNname("Bond");
        jamesBond.setGeburtsDatum(java.sql.Date.valueOf(LocalDate.of(1920, 11, 11)));
        jamesBond.setGeburtsOrt("Wattenscheid");
        jamesBond.setSchnulNetzKennzeichen("BONJ");
        jamesBond.setOffice365Kennung("james.bond@htl-wels.at");

        // Transaktion nach Persistierung

        entityManager.persist(jamesBond);
        entityManager.getTransaction().begin();
        entityManager.getTransaction().commit();

        // Synchronisierung des gesamten Persistenkontextes mittels flush()
        // Eine denkbare Anwendung wäre z.B., wenn man zwischendurch OIDs erzeugen muss
        Person kunde1 = new Person();
        kunde1.setFamilieNname("Kunde1");
        kunde1.setVorName("Hans");
        Person kunde2 = new Person();
        kunde2.setFamilieNname("Kunde2");
        kunde2.setVorName("Heinz");
        Person kunde3 = new Person();
        kunde3.setFamilieNname("Kunde3");
        kunde3.setVorName("Horst");
        Person kunde4 = new Person();
        kunde4.setFamilieNname("Kunde4");
        kunde4.setVorName("Vollhorst");

        entityManager.getTransaction().begin();
        entityManager.persist(kunde1);
        entityManager.persist(kunde2);
        entityManager.flush();
        entityManager.persist(kunde3);
        entityManager.persist(kunde4);
        entityManager.getTransaction().commit();

        // Laden von Entities mittels find
        // (dann anzuwenden, wenn man man die OID und den Typ eines Objektes kennt

        Person agent007 = entityManager.find(Person.class, 2L);
        System.out.println(agent007);

        System.out.println("***   Update eines Objektes                  ");
        // Update eines Objektes
        entityManager.getTransaction().begin();
        Person updater = new Person();
        updater.setFamilieNname("Schwips");
        updater.setVorName("Heinrich");

        updater.setGeburtsDatum(java.sql.Date.valueOf(LocalDate.of(1987, 3, 27)));
        updater.setGeburtsOrt("Wels");
        updater.setSchnulNetzKennzeichen("SchwarzH");
        updater.setOffice365Kennung("heinrich.schwarz@htl-wels.at");
        entityManager.persist(updater);
        updater.setFamilieNname("Schwarz");
        entityManager.getTransaction().commit();

        System.out.println("***   Refreshen eines Objektes                  ");

        // Rücksetzen eines Objektes auf die in der Datenbank ursprünglichen gespeicherten Daten
        entityManager.getTransaction().begin();
        Person refreshHermann = new Person();
        refreshHermann.setFamilieNname("Fleckmaier");
        refreshHermann.setVorName("Hermann");
        entityManager.persist(refreshHermann);
        entityManager.getTransaction().commit();

        refreshHermann.setFamilieNname("Mustermann");
        refreshHermann.setVorName("Manfred");
        System.out.println(refreshHermann.toString());
        entityManager.refresh(refreshHermann);
        System.out.println(refreshHermann.toString());

        System.out.println("***   Löschen eines Objektes                  ");
        System.out.println("***   Vor dem Löschen");
        System.out.println("**************************************************");

        // Löschen eines Objektes

        entityManager.getTransaction().begin();

        TypedQuery<Person> query = entityManager.createQuery("select p from Person p", Person.class);
        List<Person> personenList = query.getResultList();

        // This is pragmatic Code, not more.
        for (Person tempPerson : personenList) {
            System.out.println("***   " +  tempPerson.toString());
        }

        Person loeschKandidat1 = entityManager.find(Person.class, 3L);
        entityManager.remove(loeschKandidat1);
        Person loeschKandidat2 = entityManager.find(Person.class, 4L);
        entityManager.remove(loeschKandidat2);
        Person loeschKandidat3 = entityManager.find(Person.class, 5L);
        entityManager.remove(loeschKandidat3);
        Person loeschKandidat4 = entityManager.find(Person.class, 6L);
        entityManager.remove(loeschKandidat4);

        entityManager.getTransaction().commit();

        System.out.println("***   Nach dem Löschen");
        System.out.println("**************************************************");

        entityManager.getTransaction().begin();

        TypedQuery<Person> query2 = entityManager.createQuery("select p from Person p", Person.class);
        List<Person> personenList2 = query2.getResultList();

        // This is pragmatic Code, not more.
        for (Person tempPerson2 : personenList2) {
            System.out.println("***   " +  tempPerson2.toString());
        }

        System.out.println("**************************************************");
        entityManager.getTransaction().commit();

        entityManager.close();
    }
}
