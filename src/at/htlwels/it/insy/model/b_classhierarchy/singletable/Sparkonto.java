package at.htlwels.it.insy.model.b_classhierarchy.singletable;


import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import java.util.Objects;

@Entity
@DiscriminatorValue("Sparkonto")
public class Sparkonto extends Konto {
    @Column(precision = 5, scale =2)
    private Double zinssatz;

    public Sparkonto() {

    }
    public Double getZinssatz() {
        return zinssatz;
    }

    public void setZinssatz(Double zinssatz) {
        this.zinssatz = zinssatz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sparkonto)) return false;
        if (!super.equals(o)) return false;
        Sparkonto sparkonto = (Sparkonto) o;
        return Objects.equals(getZinssatz(), sparkonto.getZinssatz());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getZinssatz());
    }

    @Override
    public String toString() {
        return "Sparkonto{" +
                "zinssatz=" + zinssatz +
                "} " + super.toString();
    }
}
