package at.htlwels.it.insy.model.b_classhierarchy.singletable;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
@Inheritance (strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn (name = "Kontotyp", discriminatorType = DiscriminatorType.STRING)
public abstract class  Konto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private Integer kontonummer;
    @Column(precision = 10, scale =2)
    private Double kontostand;

    public Konto() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public Integer getKontonummer() {
        return kontonummer;
    }


    public void setKontonummer(Integer kontonummer) {
        this.kontonummer = kontonummer;
    }

    public Double getKontostand() {
        return kontostand;
    }

    public void setKontostand(Double kontostand) {
        this.kontostand = kontostand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Konto)) return false;
        Konto konto = (Konto) o;
        return Objects.equals(getOid(), konto.getOid()) &&
                Objects.equals(getKontonummer(), konto.getKontonummer()) &&
                Objects.equals(getKontostand(), konto.getKontostand());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getKontonummer(), getKontostand());
    }

    @Override
    public String toString() {
        return "Konto{" +
                "oid=" + oid +
                ", kontonummer=" + kontonummer +
                ", kontostand=" + kontostand +
                '}';
    }
}
