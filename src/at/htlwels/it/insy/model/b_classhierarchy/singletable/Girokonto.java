package at.htlwels.it.insy.model.b_classhierarchy.singletable;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
@DiscriminatorValue("Girokonto")
public class Girokonto extends Konto {
        @Column(precision = 5, scale =2)
        private Double sollzinssatz;
        @Column(precision = 5, scale =2)
        private Double habenzinssatz;
        @Column(precision = 10, scale =2)
        private Double kontorahmen;

    public Girokonto() {
    }

    public Double getSollzinssatz() {
        return sollzinssatz;
    }

    public void setSollzinssatz(Double sollzinssatz) {
        this.sollzinssatz = sollzinssatz;
    }

    public Double getHabenzinssatz() {
        return habenzinssatz;
    }

    public void setHabenzinssatz(Double habenzinssatz) {
        this.habenzinssatz = habenzinssatz;
    }

    public Double getKontorahmen() {
        return kontorahmen;
    }

    public void setKontorahmen(Double kontorahmen) {
        this.kontorahmen = kontorahmen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Girokonto)) return false;
        if (!super.equals(o)) return false;
        Girokonto girokonto = (Girokonto) o;
        return Objects.equals(getSollzinssatz(), girokonto.getSollzinssatz()) &&
                Objects.equals(getHabenzinssatz(), girokonto.getHabenzinssatz()) &&
                Objects.equals(getKontorahmen(), girokonto.getKontorahmen());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSollzinssatz(), getHabenzinssatz(), getKontorahmen());
    }

    @Override
    public String toString() {
        return "Girokonto{" +
                "sollzinssatz=" + sollzinssatz +
                ", habenzinssatz=" + habenzinssatz +
                ", kontorahmen=" + kontorahmen +
                "} " + super.toString();
    }
}
