package at.htlwels.it.insy.model.b_classhierarchy;

import at.htlwels.it.insy.model.b_classhierarchy.joined.EARProjekt;
import at.htlwels.it.insy.model.b_classhierarchy.joined.Projekt;
import at.htlwels.it.insy.model.b_classhierarchy.joined.WARProjekt;
import at.htlwels.it.insy.model.b_classhierarchy.singletable.Girokonto;
import at.htlwels.it.insy.model.b_classhierarchy.singletable.Konto;
import at.htlwels.it.insy.model.b_classhierarchy.singletable.Sparkonto;
import at.htlwels.it.insy.model.b_classhierarchy.tableperclass.Angestellter;
import at.htlwels.it.insy.model.b_classhierarchy.tableperclass.TeilzeitAngestellter;
import at.htlwels.it.insy.model.b_classhierarchy.tableperclass.VollzeitAngestellter;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

public class Test {

    public static void persistClassHierarchy(String PERSISTENCE_UNIT_NAME) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        // Single Table
        /*
                                  -----------------------
                                  |  Konto              |
                                  -----------------------
                                  |  Long oid           |
                                  |  Integer kontonummer|
                                  |  Long kontostand    |
                                  ----------------------
                                            ^
                                            |
                   ---------------------------------------------------
                   |                                                 |
        ----------------------                             ----------------------
        | Girokonto          |                             |  Sparkonto         |
        ----------------------                             ----------------------
        | Double sollzinsatz |                             |  Double zinssatz   |
        | Double habenzinsatz|                             ----------------------
        | Double kontorahmen |
        ----------------------

        // Konto(oid, kontonummer, kontostand, sollzinssatz, habenzinssatz, kontorahmen, zinssatz,  kontotyp)
         */

        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist a Class Hierarchy - Single Table                  ");


        entityManager.getTransaction().begin();

        Sparkonto sk = new Sparkonto();
        sk.setKontonummer(1234566666);
        sk.setZinssatz(1.0);
        sk.setKontostand(3400.01);
        entityManager.persist(sk);

        Girokonto gk = new Girokonto();
        gk.setKontonummer(1234567890);
        gk.setSollzinssatz(5.00);
        gk.setHabenzinssatz(0.01);
        gk.setKontorahmen(10000.00);
        gk.setKontostand(1234.56);
        entityManager.persist(gk);

        entityManager.getTransaction().commit();

        // Table per Class
        /*
                                  ----------------------
                                  |  Angestellter      |
                                  ----------------------
                                  |  Long oid          |
                                  |  String firstname  |
                                  |  String lastname   |
                                  |  Date birthdate    |
                                  ----------------------
                                            ^
                                            |
                   ---------------------------------------------------
                   |                                                 |
        ----------------------                             -----------------------
        |VollzeitAngestellter|                             |TeilzeitAngestellter |
        ----------------------                             -----------------------
        |Integer gehalt      |                             |Integer stundenlohn  |
        ----------------------                             |Integer wochenstunden|
                                                           -----------------------

        // VollzeitAngestellter (oid, firstname, lastname, birthdate, gehalt)
        // TeilzeitAngestellter (oid, firstname, lastname, birthdate, stundenlohn, wochenstunden)
         */

        System.out.println("***   Persist a Class Hierarchy - Table per Class              ");

        entityManager.getTransaction().begin();

        TeilzeitAngestellter karlHeinz = new TeilzeitAngestellter();
        karlHeinz.setFirstname("Karl");
        karlHeinz.setLastname("Heinz");
        karlHeinz.setBirthdate(java.sql.Date.valueOf(LocalDate.of(1986, 1, 11)));
        karlHeinz.setStundenlohn(15.00);
        karlHeinz.setWochenstunden(22);
        entityManager.persist(karlHeinz);

        VollzeitAngestellter hugoBoss = new VollzeitAngestellter();
        hugoBoss.setFirstname("Hugo");
        hugoBoss.setLastname("Boss");
        hugoBoss.setBirthdate(java.sql.Date.valueOf(LocalDate.of(1996, 4, 23)));
        hugoBoss.setGehalt(4500);
        entityManager.persist(hugoBoss);

        entityManager.getTransaction().commit();

        // Table JOINED
        /*
                                  ----------------------
                                  |  Projekt           |
                                  ----------------------
                                  |  Long oid          |
                                  |  String projektName|
                                  ----------------------
                                            ^
                                            |
                   ---------------------------------------------------
                   |                                                 |
        ----------------------                             -----------------------
        |   EARProjekt       |                             |   WARProjekt        |
        ----------------------                             -----------------------
        |   Long oid         |                             |   Long oid          |
        |   String nameAS    |                             |   String nameWC     |
        ----------------------                             -----------------------

        // Projekt(oid, projektName)
        // EARProjekt(oid, nameApplicationServer)
        // WARProjekt(oid, nameWebContainer)
        */

        System.out.println("***   Persist a Class Hierarchy - Table JOINED              ");

        entityManager.getTransaction().begin();

        EARProjekt ear = new EARProjekt();
        ear.setNameApplicationServer("Glassfish");
        entityManager.persist(ear);

        WARProjekt war = new WARProjekt();
        war.setNameWebContainer("Tomcat");
        entityManager.persist(war);

        entityManager.getTransaction().commit();

        System.out.println("**************************************************");
        System.out.println("***   Validation - Read Class Hierarchies");
        System.out.println("**************************************************");

        // Read Single Table
        entityManager.getTransaction().begin();

        TypedQuery<Konto> queryK = entityManager.createQuery("select konto from Konto konto", Konto.class);
        List<Konto> kontenList = queryK.getResultList();

        // This is pragmatic Code, not more.
        for (Konto tempKonto : kontenList) {
            System.out.println("***   " +  tempKonto.toString());
        }
        entityManager.getTransaction().commit();

        // Read Table per Class
        entityManager.getTransaction().begin();

        TypedQuery<Angestellter>  queryA = entityManager.createQuery("select angestellter from Angestellter angestellter", Angestellter.class);
        List<Angestellter> angestelltenList = queryA.getResultList();

        // This is pragmatic Code, not more.
        for (Angestellter tempAngestellter : angestelltenList) {
            System.out.println("***   " +  tempAngestellter.toString());
        }
        entityManager.getTransaction().commit();

        // Read JOINED
        entityManager.getTransaction().begin();

        TypedQuery<Projekt> query = entityManager.createQuery("select project from Projekt project", Projekt.class);
        List<Projekt> projektList = query.getResultList();

        // This is pragmatic Code, not more.
        for (Projekt tempProjekt : projektList) {
            System.out.println("***   " +  tempProjekt.toString());
        }
        System.out.println("**************************************************");
        entityManager.getTransaction().commit();

        entityManager.close();
    }
}
