package at.htlwels.it.insy.model.b_classhierarchy.joined;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Projekt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;
    private String projektName;

    public Projekt() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getProjektName() {
        return projektName;
    }

    public void setProjektName(String projektName) {
        this.projektName = projektName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Projekt projekt = (Projekt) o;

        if (oid != null ? !oid.equals(projekt.oid) : projekt.oid != null) return false;
        return projektName != null ? projektName.equals(projekt.projektName) : projekt.projektName == null;
    }

    @Override
    public int hashCode() {
        int result = oid != null ? oid.hashCode() : 0;
        result = 31 * result + (projektName != null ? projektName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Projekt{" +
                "oid=" + oid +
                ", projektName='" + projektName + '\'' +
                '}';
    }
}
