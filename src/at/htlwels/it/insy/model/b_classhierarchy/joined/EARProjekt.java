package at.htlwels.it.insy.model.b_classhierarchy.joined;

import jakarta.persistence.*;
import java.util.Objects;

@Entity

public class EARProjekt extends Projekt {

    private String nameApplicationServer;

    public EARProjekt() {
    }

    public String getNameApplicationServer() {
        return nameApplicationServer;
    }

    public void setNameApplicationServer(String nameApplicationServer) {
        this.nameApplicationServer = nameApplicationServer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EARProjekt)) return false;
        if (!super.equals(o)) return false;
        EARProjekt that = (EARProjekt) o;
        return Objects.equals(getNameApplicationServer(), that.getNameApplicationServer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNameApplicationServer());
    }

    @Override
    public String toString() {
        return "EARProject{" +
                "oid=" + super.getOid() +
                ", nameApplicationServer='" + nameApplicationServer + '\'' +
                "} " + super.toString();
    }
}
