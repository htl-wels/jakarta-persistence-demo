package at.htlwels.it.insy.model.b_classhierarchy.joined;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
public class WARProjekt extends Projekt {

    private String nameWebContainer;

    public String getNameWebContainer() {
        return nameWebContainer;
    }

    public void setNameWebContainer(String nameWebContainer) {
        this.nameWebContainer = nameWebContainer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WARProjekt)) return false;
        if (!super.equals(o)) return false;
        WARProjekt that = (WARProjekt) o;
        return Objects.equals(getNameWebContainer(), that.getNameWebContainer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), super.getOid(), getNameWebContainer());
    }

    @Override
    public String toString() {
        return "WARProject{" +
                "oid=" + super.getOid() +
                ", nameWebContainer='" + nameWebContainer + '\'' +
                "} " + super.toString();
    }
}
