package at.htlwels.it.insy.model.b_classhierarchy.tableperclass;

import jakarta.persistence.Entity;
import java.util.Objects;

@Entity
public class TeilzeitAngestellter extends Angestellter {

    private Integer wochenstunden;
    private Double stundenlohn;

    public TeilzeitAngestellter() {
    }

    public Integer getWochenstunden() {
        return wochenstunden;
    }

    public void setWochenstunden(Integer wochenstunden) {
        this.wochenstunden = wochenstunden;
    }

    public Double getStundenlohn() {
        return stundenlohn;
    }

    public void setStundenlohn(Double stundenlohn) {
        this.stundenlohn = stundenlohn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TeilzeitAngestellter)) return false;
        TeilzeitAngestellter that = (TeilzeitAngestellter) o;
        return Objects.equals(getWochenstunden(), that.getWochenstunden()) &&
                Objects.equals(getStundenlohn(), that.getStundenlohn());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getWochenstunden(), getStundenlohn());
    }

    @Override
    public String toString() {
        return "TeilzeitAngestellter{" +
                "wochenstunden=" + wochenstunden +
                ", stundenlohn=" + stundenlohn +
                "} " + super.toString();
    }
}
