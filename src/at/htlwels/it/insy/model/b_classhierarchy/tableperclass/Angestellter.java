package at.htlwels.it.insy.model.b_classhierarchy.tableperclass;

import jakarta.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Angestellter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String firstname;
    private String lastname;
    private Date birthdate;

    public Angestellter() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Angestellter)) return false;
        Angestellter that = (Angestellter) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getFirstname(), that.getFirstname()) &&
                Objects.equals(getLastname(), that.getLastname()) &&
                Objects.equals(getBirthdate(), that.getBirthdate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getFirstname(), getLastname(), getBirthdate());
    }

    @Override
    public String toString() {
        return "Angestellter{" +
                "oid=" + oid +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
