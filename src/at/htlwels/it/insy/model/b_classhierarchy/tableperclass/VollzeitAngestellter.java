package at.htlwels.it.insy.model.b_classhierarchy.tableperclass;

import jakarta.persistence.Entity;
import java.util.Objects;

@Entity
public class VollzeitAngestellter extends Angestellter {

    private Integer gehalt;

    public VollzeitAngestellter() {

    }

    public Integer getGehalt() {
        return gehalt;
    }

    public void setGehalt(Integer gehalt) {
        this.gehalt = gehalt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VollzeitAngestellter)) return false;
        if (!super.equals(o)) return false;
        VollzeitAngestellter that = (VollzeitAngestellter) o;
        return Objects.equals(getGehalt(), that.getGehalt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getGehalt());
    }

    @Override
    public String toString() {
        return "VollzeitAngestellter{" +
                "gehalt=" + gehalt +
                "} " + super.toString();
    }
}
