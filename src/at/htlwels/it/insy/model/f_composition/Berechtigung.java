package at.htlwels.it.insy.model.f_composition;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.Objects;

@Entity
public class Berechtigung {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String name;

    public Berechtigung() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Berechtigung)) return false;
        Berechtigung that = (Berechtigung) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getName());
    }

    @Override
    public String toString() {
        return "Berechtigung{" +
                "oid=" + oid +
                ", name='" + name + '\'' +
                '}';
    }
}
