package at.htlwels.it.insy.model.f_composition;

import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Rolle {


    // Eine Komposition ist wie eine unidirektionale OneToMany-Beziehung zu modellieren.
    // Durch den CascadeType.All wird festgelegt, dass automatisch, wenn das Basis-Objekt
    // gelöscht wird, auch die Ziel-Objekt gelöscht werden.

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Berechtigung> berechtigungen;

    public Rolle() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Berechtigung> getBerechtigungen() {
        return berechtigungen;
    }

    public void setBerechtigungen(List<Berechtigung> berechtigungen) {
        this.berechtigungen = berechtigungen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rolle)) return false;
        Rolle rolle = (Rolle) o;
        return Objects.equals(getOid(), rolle.getOid()) &&
                Objects.equals(getName(), rolle.getName()) &&
                Objects.equals(getBerechtigungen(), rolle.getBerechtigungen());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getName(), getBerechtigungen());
    }

    @Override
    public String toString() {
        return "Rolle{" +
                "oid=" + oid +
                ", name='" + name + '\'' +
                ", berechtigungen=" + berechtigungen +
                '}';
    }


}
