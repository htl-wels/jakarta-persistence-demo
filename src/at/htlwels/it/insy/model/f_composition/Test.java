package at.htlwels.it.insy.model.f_composition;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void persistComposition(String PERSISTENCE_UNIT_NAME) {

        // Composition
        /*
        ----------------------                       -----------------------
        |                    |                    *  |                     |
        |       Rolle        |◆--------------------->|    Berechtigung     |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist an Association - Composition        ");
        System.out.println("**************************************************");

        entityManager.getTransaction().begin();

        Rolle rudisBerechtigungen = new Rolle();
        rudisBerechtigungen.setName("RudiDerSchulwart");
        List<Berechtigung> dasDarfRudiTun= new ArrayList<>();
        rudisBerechtigungen.setBerechtigungen(dasDarfRudiTun);

        Berechtigung schuelerBeimRauchenSchimpfen = new Berechtigung();
        schuelerBeimRauchenSchimpfen.setName("Schueler beim Rauchen schimpfen");
        rudisBerechtigungen.getBerechtigungen().add(schuelerBeimRauchenSchimpfen);

        Berechtigung mitMonikaDiskutieren = new Berechtigung();
        mitMonikaDiskutieren.setName("Mit Monika diskutieren");
        rudisBerechtigungen.getBerechtigungen().add(mitMonikaDiskutieren);

        Berechtigung aulaTischeKontrollieren = new Berechtigung();
        aulaTischeKontrollieren.setName("Aulatische kontrollieren");
        rudisBerechtigungen.getBerechtigungen().add(aulaTischeKontrollieren);

        entityManager.persist(rudisBerechtigungen);

        entityManager.getTransaction().commit();
        entityManager.clear();

        // Read OnetoMany Unidirectional Association

        System.out.println("**************************************************");
        System.out.println("***   Validation - Composition");
        System.out.println("**************************************************");

        entityManager.getTransaction().begin();

        TypedQuery<Rolle> query = entityManager.createQuery("select r from Rolle r", Rolle.class);

        Rolle rudi =  query.getSingleResult();
        System.out.printf("***   %s%n", rudi.toString());
        System.out.println("***   Anzahl der Berechtigungen: " + rudi.getBerechtigungen().size());

        entityManager.getTransaction().commit();
        System.out.println("**************************************************");

        entityManager.close();
    }
}
