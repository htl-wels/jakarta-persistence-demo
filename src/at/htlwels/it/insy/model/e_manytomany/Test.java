package at.htlwels.it.insy.model.e_manytomany;

import at.htlwels.it.insy.model.e_manytomany.bidirectional.Autor;
import at.htlwels.it.insy.model.e_manytomany.bidirectional.Buch;
import at.htlwels.it.insy.model.e_manytomany.unidirectional.Film;
import at.htlwels.it.insy.model.e_manytomany.unidirectional.SuperHero;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void persistManyToManyRelationship(String PERSISTENCE_UNIT_NAME) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        // ManyToMany - Unidirectional
        /*
        ----------------------                       -----------------------
        |                    |                    *  |                     |
        |      Film          |---------------------->|     SuperHero       |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */

        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist an Association - ManyToMany - Unidirectional                  ");

        entityManager.getTransaction().begin();

        SuperHero ironman = new SuperHero();
        ironman.setName("Ironman");

        SuperHero thor = new SuperHero();
        thor.setName("Thor");

        Film avengers = new Film();
        avengers.setTitel("Avengers");
        avengers.setErscheinungsDatum(java.sql.Date.valueOf(LocalDate.of(2008, 2, 12)));
        List<SuperHero> avengersSuperHeroes = new ArrayList<>();
        avengersSuperHeroes.add(ironman);
        avengersSuperHeroes.add(thor);
        avengers.setSuperHeros(avengersSuperHeroes);

        Film infinityWar = new Film();
        infinityWar.setTitel("Infinity War");
        infinityWar.setErscheinungsDatum(java.sql.Date.valueOf(LocalDate.of(2017, 12, 12)));
        List<SuperHero> infinityWarSuperHeroes = new ArrayList<>();
        infinityWarSuperHeroes.add(ironman);
        infinityWarSuperHeroes.add(thor);
        infinityWar.setSuperHeros(infinityWarSuperHeroes);

        entityManager.persist(avengers);
        entityManager.persist(infinityWar);

        entityManager.getTransaction().commit();

        // ManyToMany - Bidirectional
        /*
        ----------------------                       -----------------------
        |                    |  *                 *  |                     |
        |     Autor          |-----------------------|        Buch         |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */

        System.out.println("***   Persist an Association - ManyToMany - Bidirectional                  ");

        entityManager.getTransaction().begin();

        // Anlage Bücher
        Buch designPatterns = new Buch();
        designPatterns.setBuchTitel("Design Patterns");
        List<Autor> autorenDesignPattern = new ArrayList<>();
        designPatterns.setAutoren(autorenDesignPattern);

        Buch contributingToEclipse = new Buch();
        contributingToEclipse.setBuchTitel("Contributing to Eclipse");
        List<Autor> autorenContributingToEclipse = new ArrayList<>();
        contributingToEclipse.setAutoren(autorenContributingToEclipse);

        Buch patternHatching = new Buch();
        patternHatching.setBuchTitel("Pattern Hatching");
        List<Autor> autorenPatternHatching = new ArrayList<>();
        patternHatching.setAutoren(autorenPatternHatching);

        // Anlage Autoren und zuweisen der Bücher
        Autor erichGamma = new Autor();
        erichGamma.setVorname("Erich");
        erichGamma.setNachname("Gamma");
        List<Buch> erichGammaBuecher = new ArrayList<>();
        erichGamma.setBuecher(erichGammaBuecher);
        erichGammaBuecher.add(designPatterns);
        erichGammaBuecher.add(contributingToEclipse);

        Autor richardHelm = new Autor();
        richardHelm.setVorname("Richard");
        richardHelm.setNachname("Helm");
        List<Buch> richardHelmBuecher = new ArrayList<>();
        richardHelm.setBuecher(richardHelmBuecher);
        richardHelmBuecher.add(designPatterns);

        Autor ralphJohnson = new Autor();
        ralphJohnson.setVorname("Ralph");
        ralphJohnson.setNachname("Johnson");
        List<Buch> ralphJohnsonBuecher = new ArrayList<>();
        ralphJohnson.setBuecher(ralphJohnsonBuecher);
        ralphJohnsonBuecher.add(designPatterns);

        Autor johnVlissides = new Autor();
        johnVlissides.setVorname("John");
        johnVlissides.setNachname("Vlissides");
        List<Buch> johnVlissidesBuecher = new ArrayList<>();
        johnVlissides.setBuecher(johnVlissidesBuecher);
        johnVlissidesBuecher.add(designPatterns);
        johnVlissidesBuecher.add(patternHatching);

        // Setzen der Autoren
        designPatterns.getAutoren().add(erichGamma);
        designPatterns.getAutoren().add(richardHelm);
        designPatterns.getAutoren().add(ralphJohnson);
        designPatterns.getAutoren().add(johnVlissides);

        contributingToEclipse.getAutoren().add(erichGamma);

        patternHatching.getAutoren().add(johnVlissides);

        entityManager.persist(erichGamma);
        entityManager.persist(richardHelm);
        entityManager.persist(ralphJohnson);
        entityManager.persist(johnVlissides);

        entityManager.getTransaction().commit();
        entityManager.clear();

        System.out.println("**************************************************");
        System.out.println("***   Validation - Read ManyToMany Associations");
        System.out.println("**************************************************");

        // Read ManyToMany Unidirectional Association

        entityManager.getTransaction().begin();

        TypedQuery<Film> queryF = entityManager.createQuery("select f from Film f", Film.class);

        List<Film> filmList = queryF.getResultList();

        // This is pragmatic Code, not more.
        for (Film tempFilm : filmList) {
            System.out.println("***   " +  tempFilm.toString());
        }

        entityManager.getTransaction().commit();


        // Read ManyToMany Bidirectional Association
        entityManager.getTransaction().begin();

        TypedQuery<Autor> queryA = entityManager.createQuery("select a from Autor a", Autor.class);

        List<Autor> autorenList = queryA.getResultList();

        // This is pragmatic Code, not more.
        for (Autor tempAutor : autorenList) {
            System.out.println("***   " +  tempAutor.toString());
        }

        entityManager.getTransaction().commit();
        entityManager.clear();

        entityManager.getTransaction().begin();

        TypedQuery<Buch> queryB = entityManager.createQuery("select b from Buch b", Buch.class);

        List<Buch> buecherList = queryB.getResultList();

        // This is pragmatic Code, not more.
        for (Buch tempBuch : buecherList) {
            System.out.println("***   " +  tempBuch.toString());
        }

        entityManager.getTransaction().commit();
        System.out.println("**************************************************");

        entityManager.close();
    }
}
