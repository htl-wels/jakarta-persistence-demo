package at.htlwels.it.insy.model.e_manytomany.bidirectional;

import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Buch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String buchTitel;

    @ManyToMany(mappedBy = "buecher")
    private List<Autor> autoren;

    public Buch() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getBuchTitel() {
        return buchTitel;
    }

    public void setBuchTitel(String buchTitel) {
        this.buchTitel = buchTitel;
    }


    public List<Autor> getAutoren() {
        return autoren;
    }

    public void setAutoren(List<Autor> autoren) {
        this.autoren = autoren;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buch)) return false;
        Buch buch = (Buch) o;
        return Objects.equals(getOid(), buch.getOid()) &&
                Objects.equals(getBuchTitel(), buch.getBuchTitel()) &&
                Objects.equals(getAutoren(), buch.getAutoren());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getBuchTitel(), getAutoren());
    }

    @Override
    public String toString() {
        return "Buch{" +
                "oid=" + oid +
                ", buchTitel='" + buchTitel + '\'' +
                '}';
    }
}
