package at.htlwels.it.insy.model.e_manytomany.bidirectional;

import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Autor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String vorname;
    private String nachname;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "AUTOR_BUCH",
            joinColumns = {@JoinColumn (name = "AutorOID")},
            inverseJoinColumns =  {@JoinColumn(name = "BuchOID")}
    )
    private List<Buch> buecher;

    public Autor() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public List<Buch> getBuecher() {
        return buecher;
    }

    public void setBuecher(List<Buch> buecher) {
        this.buecher = buecher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Autor)) return false;
        Autor autor = (Autor) o;
        return Objects.equals(getOid(), autor.getOid()) &&
                Objects.equals(getVorname(), autor.getVorname()) &&
                Objects.equals(getNachname(), autor.getNachname()) &&
                Objects.equals(getBuecher(), autor.getBuecher());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getVorname(), getNachname(), getBuecher());
    }

    @Override
    public String toString() {
        return "Autor{" +
                "oid=" + oid +
                ", vorname='" + vorname + '\'' +
                ", nachname='" + nachname + '\'' +
                '}';
    }
}
