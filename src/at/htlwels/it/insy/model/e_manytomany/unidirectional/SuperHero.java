package at.htlwels.it.insy.model.e_manytomany.unidirectional;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.util.Objects;

@Entity
public class SuperHero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String name;

    public SuperHero() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SuperHero)) return false;
        SuperHero superHero = (SuperHero) o;
        return Objects.equals(getOid(), superHero.getOid()) &&
                Objects.equals(getName(), superHero.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getName());
    }

    @Override
    public String toString() {
        return "SuperHero{" +
                "oid=" + oid +
                ", name='" + name + '\'' +
                '}';
    }
}
