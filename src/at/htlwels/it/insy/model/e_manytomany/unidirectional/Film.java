package at.htlwels.it.insy.model.e_manytomany.unidirectional;

import jakarta.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String titel;
    private Date erscheinungsDatum;

    @ManyToMany (cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "FILM_SUPERHERO",
            joinColumns = {@JoinColumn (name = "FilmOID")},
                    inverseJoinColumns =  {@JoinColumn(name = "SuperHeroOID")}
    )
    private List<SuperHero> superHeros;

    public Film() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public Date getErscheinungsDatum() {
        return erscheinungsDatum;
    }

    public void setErscheinungsDatum(Date erscheinungsDatum) {
        this.erscheinungsDatum = erscheinungsDatum;
    }

    public List<SuperHero> getSuperHeros() {
        return superHeros;
    }

    public void setSuperHeros(List<SuperHero> superHeros) {
        this.superHeros = superHeros;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Film)) return false;
        Film film = (Film) o;
        return Objects.equals(getOid(), film.getOid()) &&
                Objects.equals(getTitel(), film.getTitel()) &&
                Objects.equals(getErscheinungsDatum(), film.getErscheinungsDatum()) &&
                Objects.equals(getSuperHeros(), film.getSuperHeros());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getTitel(), getErscheinungsDatum(), getSuperHeros());
    }

    @Override
    public String toString() {
        return "Film{" +
                "oid=" + oid +
                ", titel='" + titel + '\'' +
                ", erscheinungsDatum=" + erscheinungsDatum +
                '}';
    }
}
