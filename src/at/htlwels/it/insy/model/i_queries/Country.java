package at.htlwels.it.insy.model.i_queries;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name="Country.findAll", query = "SELECT c FROM Country c"),
        @NamedQuery(name="Country.findByName", query = "SELECT c FROM Country c WHERE c.name = :name"),
})
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String name;
    private String capital;
    private Double area;
    private Double population;

    // Wir gehen der Einfacheit davon aus, dass jedes Land eindeutig auf einem Kontinent liegt.
    @ManyToOne
    @JoinColumn(name = "continentOID", nullable = false)
    private Continent continent;


    public Country() {
    }

    public Country (Continent theContinent, String nameOfCountry, String cap, Double ar, Double pop) {
        this.setContinent(theContinent);
        this.setName(nameOfCountry);
        this.setCapital(cap);
        this.setArea(ar);
        this.setPopulation(pop);
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getPopulation() {
        return population;
    }

    public void setPopulation(Double population) {
        this.population = population;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }


    public boolean lessArea(Country country2) {
        return this.area < country2.area;
    }


    public boolean lessPopulation(Country country2) {
        return population < country2.population;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Objects.equals(getOid(), country.getOid()) &&
                Objects.equals(getName(), country.getName()) &&
                Objects.equals(getCapital(), country.getCapital()) &&
                Objects.equals(getArea(), country.getArea()) &&
                Objects.equals(getPopulation(), country.getPopulation()) &&
                Objects.equals(getContinent(), country.getContinent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getName(), getCapital(), getArea(), getPopulation(), getContinent());
    }

    @Override
    public String toString() {
        return "Country{" +
                "oid=" + oid +
                ", name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", area=" + area +
                ", population=" + population +
                ", continent=" + continent +
                '}';
    }
}
