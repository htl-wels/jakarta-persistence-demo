package at.htlwels.it.insy.model.i_queries;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Continent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String name;

    // eh schon wissen, alle Persistenz-Mechanismen werden auf alle Objekte in der Liste weitergegeben
    @OneToMany(mappedBy = "continent", cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Country> countries;


    public Continent() {
    }

    public Continent(String theName) {
        name = theName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public void addCountry(Country country) {
        if (countries == null) {
            countries = new ArrayList<>();
        }
        countries.add(country);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Continent)) return false;
        Continent continent = (Continent) o;
        return Objects.equals(oid, continent.oid) &&
                Objects.equals(getName(), continent.getName()) &&
                Objects.equals(getCountries(), continent.getCountries());
    }

    @Override
    public int hashCode() {
        return Objects.hash(oid, getName(), getCountries());
    }

    @Override
    public String toString() {
        return "Continent{" +
                "oid=" + oid +
                ", name='" + name + '\'' +
                '}';
    }
}
