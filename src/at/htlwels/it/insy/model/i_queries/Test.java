package at.htlwels.it.insy.model.i_queries;

import jakarta.persistence.*;
import java.util.List;

public class Test {

    public static void showQueries(String PERSISTENCE_UNIT_NAME) {

        // Gibt Überblick über die Möglichkeiten von Queries
        // Dieses Thema ist sehr umfangreich, daher werden hier nur die wichtigsten Funktionaliäten exemplarisch
        // dargestellt!

        // Recursion

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Show Query Results               ");

        entityManager.getTransaction().begin();

        Continent europe = new Continent("Europe");
        Continent asia = new Continent("Asia");
        Continent america = new Continent("America");

        Country us = new Country(america, "USA", "Washington", 3787.3D, 300.5D);
        Country canada = new Country(america, "Canada", "Ottawa", 3849D, 28D);
        Country china = new Country(asia, "China", "Beijing", 3696D, 1203D);
        Country france = new Country(europe, "France", "Paris", 210D, 56D);
        Country italy = new Country(europe, "Italy", "Rome", 116.4D, 58.9D);
        Country greece = new Country(europe, "Greece", "Athens", 131D, 11D);
        Country austria = new Country(europe, "Austria", "Vienna", 83D, 8.3D);
        Country belgium = new Country(europe, "Belgium", "Brussels", 30D, 10D);
        Country cyprus = new Country(europe, "Cyprus", "Nicosia", 9D, 0.8D);
        Country estonia = new Country(europe, "Estonia", "Tallinn", 45D, 1.3D);
        Country finland = new Country(europe, "Finland", "Helsinki", 338D, 5.3D);
        Country germany = new Country(europe, "Germany", "Berlin", 356D, 82D);
        Country irland = new Country(europe, "Ireland", "Dublin", 70D, 4.5D);
        Country luxembourg = new Country(europe, "Luxembourg", "Luxembourg", 2D, 0.5D);
        Country malta = new Country(europe, "Malta", "Malletta", 0.3D, 0.4D);
        Country netherlands = new Country(europe, "Netherlands", "Amsterdam", 41D, 16D);
        Country portugal = new Country(europe, "Portugal", "Lisbon", 92D, 10.6D);
        Country slovakia = new Country(europe, "Slovakia", "Bratislava", 48D, 5.4D);
        Country slovenia = new Country(europe, "Slovenia", "Ljubljiana", 20D, 2D);
        Country spain =  new Country(europe, "Spain", "Madrid", 504D, 45D);
        Country korea = new Country (asia, "Korea", "Seul", 800D, 90D);
        Country costarica = new Country(america, "Costa Rica", "San Jose", 52D, 84D);
        Country  chile = new Country(america, "Chile", "Santiago", 756D, 18D);

        asia.addCountry(china);
        asia.addCountry(korea);
        america.addCountry(us);
        america.addCountry(canada);
        america.addCountry(costarica);
        america.addCountry(chile);
        europe.addCountry(france);
        europe.addCountry(italy);
        europe.addCountry(greece);
        europe.addCountry(austria);
        europe.addCountry(belgium);
        europe.addCountry(cyprus);
        europe.addCountry(estonia);
        europe.addCountry(finland);
        europe.addCountry(germany);
        europe.addCountry(irland);
        europe.addCountry(luxembourg);
        europe.addCountry(malta);
        europe.addCountry(netherlands);
        europe.addCountry(portugal);
        europe.addCountry(slovakia);
        europe.addCountry(slovenia);
        europe.addCountry(spain);

        entityManager.persist(america);
        entityManager.persist(asia);
        entityManager.persist(europe);

        entityManager.getTransaction().commit();

        // Der ganze Sche.. ist gespeichert
        // Löschen des Persistent-Kontextes
        entityManager.clear();

        // Lesen eines Objektes, von dem man die OID kennt.
        entityManager.getTransaction().begin();
        Country at  = entityManager.find(Country.class, 5L);
        System.out.println(at);
        entityManager.getTransaction().commit();
        entityManager.clear();

        // Query und TypedQuery (Typed Query ist zu bevorzugen)
        Query q1 = entityManager.createQuery("SELECT c from Country c");
        // Diese Query erzeugt eine Unchecked Cast Warning
        List<Country> list1 = (List<Country>) q1.getResultList();
        System.out.println("Query: Anzahl der gefundenen Elemente: " + list1.size());
        System.out.println("**************************************************");

        TypedQuery<Country> q2 = entityManager.createQuery("SELECT c from Country c", Country.class);
        List<Country> list2 = q2.getResultList();
        System.out.println("TypedQuery: Anzahl der gefundenen Elemente: " + list2.size());
        System.out.println("**************************************************");

        // Wenn nur ein einziges Objekt zurückgegeben wird, dann kann statt ResultList SingleResult verwendet werden.
        TypedQuery<Long> q3 = entityManager.createQuery("SELECT COUNT(c) FROM Country c", Long.class);
        Long countryCount = q3.getSingleResult();
        System.out.println("SingleResult: Anzahl der gefundenen Elemente: " + countryCount);
        System.out.println("**************************************************");

        // Named Parameters
        TypedQuery<Country> q4 = entityManager.createQuery("SELECT c FROM Country c WHERE c.name = :name", Country.class);
        Country temp4 = q4.setParameter("name", "Austria").getSingleResult();
        System.out.println("Named Parameter: " + temp4.toString());
        System.out.println("**************************************************");

        // Named Queries - Siehe Annotation in der Klasse Country
        TypedQuery<Country> q5 = entityManager.createNamedQuery("Country.findAll", Country.class);
        List<Country> list5 = q5.getResultList();
        System.out.println("Named Query: Anzahl der gefundenen Elemente: " + list5.size());
        System.out.println("**************************************************");

        // Projektion auf Ländernamen
        TypedQuery<String> q6 = entityManager.createQuery("SELECT c.name FROM Country AS c", String.class);
        List<String> list6 = q6.getResultList();
        System.out.print("Ländernamen: ");
        for (String countryString : list6) {
            System.out.print(countryString+", ");
        }
        System.out.print(" ...");
        System.out.println();
        System.out.println("**************************************************");

        // SELECT DISTINCT und ORDER BY
        TypedQuery<Continent> q7 = entityManager.createQuery("SELECT DISTINCT c.continent from Country AS  c WHERE c.name LIKE 'C%' ORDER BY c.continent.name", Continent.class);
        List<Continent> list7 = q7.getResultList();
        for (Continent cont : list7) {
            System.out.println(cont.toString());
        }
        System.out.println("**************************************************");

        // SELECT über einzelne Spalten
        TypedQuery<Object[]> q8 = entityManager.createQuery("SELECT UPPER(c.continent.name), c.name, c.capital from Country  c  ORDER BY c.continent.name, c.name", Object[].class);
        List<Object[]> list8 = q8.getResultList();

        for (Object[] result: list8) {
            System.out.println("Continent: " + result[0] + "  Country: " + result[1] + "  Capitol: " + result[2] );
        }
        System.out.println("**************************************************");

        // Aggregate Funktion
        TypedQuery<Double> q9 = entityManager.createQuery("SELECT Max(c.population) from Country c", Double.class);
        Double groessteBevoelkerungsAnzahlInEinemLand = q9.getSingleResult();

        System.out.println("Groesste Bevoelkerungsanzahl in einem Land: " + groessteBevoelkerungsAnzahlInEinemLand);
        System.out.println("**************************************************");

        // Between
        TypedQuery<Country> q10 = entityManager.createQuery("SELECT c FROM Country c WHERE c.area BETWEEN 100 AND 999 ORDER BY c.area", Country.class);
        List<Country> list10 = q10.getResultList();
        for (Country c10 : list10) {
            System.out.println(c10.toString());
        }
        System.out.println("**************************************************");

        // Like
        TypedQuery<Country> q11 = entityManager.createQuery("SELECT c FROM Country c WHERE c.name LIKE 'S%'", Country.class);
        List<Country> list11 = q11.getResultList();
        for (Country c11 : list11) {
            System.out.println(c11.toString());
        }
        System.out.println("**************************************************");

        entityManager.close();
    }
}
