package at.htlwels.it.insy.model.d_onetomany.unidirectional;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Schueler {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String vorname;
    private String familienname;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Sozialdienst> sozialdienste = new ArrayList<>();

    public Schueler() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getFamilienname() {
        return familienname;
    }

    public void setFamilienname(String familienname) {
        this.familienname = familienname;
    }

    public List<Sozialdienst> getSozialdienste() {
        return sozialdienste;
    }

    public void setSozialdienste(List<Sozialdienst> sozialdienste) {
        this.sozialdienste = sozialdienste;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Schueler)) return false;
        Schueler schueler = (Schueler) o;
        return Objects.equals(getOid(), schueler.getOid()) &&
                Objects.equals(getVorname(), schueler.getVorname()) &&
                Objects.equals(getFamilienname(), schueler.getFamilienname()) &&
                Objects.equals(getSozialdienste(), schueler.getSozialdienste());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getVorname(), getFamilienname(), getSozialdienste());
    }

    @Override
    public String toString() {
        return "Schueler{" +
                "oid=" + oid +
                ", vorname='" + vorname + '\'' +
                ", familienname='" + familienname + '\'' +
                ", sozialdienste=" + sozialdienste +
                '}';
    }
}
