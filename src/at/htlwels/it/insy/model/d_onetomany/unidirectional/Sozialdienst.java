package at.htlwels.it.insy.model.d_onetomany.unidirectional;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Sozialdienst {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String bezeichnung;
    private Date datum;

    public Sozialdienst() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sozialdienst)) return false;
        Sozialdienst that = (Sozialdienst) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getBezeichnung(), that.getBezeichnung()) &&
                Objects.equals(getDatum(), that.getDatum());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getBezeichnung(), getDatum());
    }

    @Override
    public String toString() {
        return "Sozialdienst{" +
                "oid=" + oid +
                ", bezeichnung='" + bezeichnung + '\'' +
                ", datum=" + datum +
                '}';
    }
}
