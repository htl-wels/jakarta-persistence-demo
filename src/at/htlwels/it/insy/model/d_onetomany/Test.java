package at.htlwels.it.insy.model.d_onetomany;

import at.htlwels.it.insy.model.d_onetomany.bidirectional.Bankkonto;
import at.htlwels.it.insy.model.d_onetomany.bidirectional.Buchung;
import at.htlwels.it.insy.model.d_onetomany.unidirectional.Schueler;
import at.htlwels.it.insy.model.d_onetomany.unidirectional.Sozialdienst;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.ArrayList;

public class Test {

    public static void persistOneToManyRelationship(String PERSISTENCE_UNIT_NAME) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        // OneToMany - Unidirectional
        /*
        ----------------------                       -----------------------
        |                    |                    *  |                     |
        |     Schueler       |---------------------->|    Sozialdienst     |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */
        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist an Association - OneToMany - Unidirectional                  ");

        entityManager.getTransaction().begin();
        Schueler theodorEgal = new Schueler();
        theodorEgal.setFamilienname("Egal");
        theodorEgal.setVorname("Theodor");
        ArrayList<Sozialdienst> sozialdienste;
        sozialdienste = new ArrayList<>();
        theodorEgal.setSozialdienste(sozialdienste);

        Sozialdienst sd1 = new Sozialdienst();
        sd1.setBezeichnung("Pausenaufsicht");
        sd1.setDatum(java.sql.Date.valueOf(LocalDate.of(2020, 2, 12)));
        sozialdienste.add(sd1);

        Sozialdienst sd2 = new Sozialdienst();
        sd2.setBezeichnung("Aussenreinigung");
        sd2.setDatum(java.sql.Date.valueOf(LocalDate.of(2020, 2, 11)));
        sozialdienste.add(sd2);

        Sozialdienst sd3 = new Sozialdienst();
        sd3.setBezeichnung("Pizzaservice");
        sd3.setDatum(java.sql.Date.valueOf(LocalDate.of(2020, 2, 1)));
        sozialdienste.add(sd3);

        entityManager.persist(theodorEgal);

        entityManager.getTransaction().commit();

        // OneToMany - Bidirectional
        /*
        ----------------------                       -----------------------
        |                    |  1                 *  |                     |
        |     Bankkonto      |-----------------------|       Buchung       |
        |                    |                       |                     |
        ----------------------                       -----------------------
         */
        System.out.println("***   Persist an Association - OneToMany - Bidirectional                  ");
        Bankkonto bankkonto = new Bankkonto();
        bankkonto.setKontoInhaber("Ritchi Rich");
        bankkonto.setKontoNummer("123456778");
        bankkonto.setKontoStand(67890.12);
        ArrayList<Buchung> buchungen;
        buchungen = new ArrayList<>();
        bankkonto.setBuchungen(buchungen);

        Buchung b1 = new Buchung();
        b1.setBetrag(123.23);
        b1.setBuchungsdatum(java.sql.Date.valueOf(LocalDate.of(2020, 10, 12)));
        b1.setBuchungstext("RNR: 1234");
        b1.setBankkonto(bankkonto);
        buchungen.add(b1);

        Buchung b2 = new Buchung();
        b2.setBetrag(777.23);
        b2.setBuchungsdatum(java.sql.Date.valueOf(LocalDate.of(2020, 2, 22)));
        b2.setBuchungstext("RNR: 1235");
        b2.setBankkonto(bankkonto);
        buchungen.add(b2);

        Buchung b3 = new Buchung();
        b3.setBetrag(1267.23);
        b3.setBuchungsdatum(java.sql.Date.valueOf(LocalDate.of(2020, 2, 23)));
        b3.setBuchungstext("RNR: 1236");
        b3.setBankkonto(bankkonto);
        buchungen.add(b3);

        // Beispiel dafür, dass die Erzeugung von Objekten nicht in der Transaktionsklammer erfolgen muss
        entityManager.getTransaction().begin();
        entityManager.persist(b1);
        entityManager.persist(b2);
        entityManager.persist(b3);
        entityManager.persist(bankkonto);

        entityManager.getTransaction().commit();

        System.out.println("**************************************************");
        System.out.println("***   Validation - Read OneToMany Associations");
        System.out.println("**************************************************");

        // Read OnetoMany Unidirectional Association
        entityManager.getTransaction().begin();

        TypedQuery<Schueler> queryT1 = entityManager.createQuery("select s from Schueler s", Schueler.class);

        Schueler newSchueler =  queryT1.getSingleResult();
        System.out.printf("***   %s%n", newSchueler.toString());
        System.out.println("***   Anzahl der Sozialdienste des Schuelers: " + newSchueler.getSozialdienste().size());

        entityManager.getTransaction().commit();

        // Löscht den Cache der materialisierten Objekte
        entityManager.clear();

        // Read OnetoMany Bidirectional Association
        entityManager.getTransaction().begin();

        TypedQuery<Bankkonto> queryB = entityManager.createQuery("select k from Bankkonto k", Bankkonto.class);
        Bankkonto theBankkonto =  queryB.getSingleResult();

        System.out.println("***   " + theBankkonto.toString());
        System.out.println("***   Anzahl der Buchungen des Bankkontos (Soll: 3) Ist: " + theBankkonto.getBuchungen().size());
        System.out.println();
        System.out.println("**************************************************");

        entityManager.getTransaction().commit();

        entityManager.close();
    }
}
