package at.htlwels.it.insy.model.d_onetomany.bidirectional;

import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Bankkonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String kontoNummer;
    private String kontoInhaber;
    private Double kontoStand;

    // Hier ist kein fetchType und auch kein CascadeType gesetzt.
    // Dies bedeutet, dass die Buchungen nicht automatisch gelesen werden und auch keine Buchungen, die
    // am Konto dranhängen automatisch gespeichert werden. Ich muss mich selber darum kümmern, dass
    // die Buchungen gelesen werden, bzw. auch gespeichert werden.
    @OneToMany(mappedBy = "bankkonto", fetch = FetchType.LAZY)
    private List<Buchung> buchungen;

    public Bankkonto() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getKontoNummer() {
        return kontoNummer;
    }

    public void setKontoNummer(String kontoNummer) {
        this.kontoNummer = kontoNummer;
    }

    public String getKontoInhaber() {
        return kontoInhaber;
    }

    public void setKontoInhaber(String kontoInhaber) {
        this.kontoInhaber = kontoInhaber;
    }

    public Double getKontoStand() {
        return kontoStand;
    }

    public void setKontoStand(Double kontoStand) {
        this.kontoStand = kontoStand;
    }

    public List<Buchung> getBuchungen() {
        return buchungen;
    }

    public void setBuchungen(List<Buchung> buchungen) {
        this.buchungen = buchungen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bankkonto)) return false;
        Bankkonto bankkonto = (Bankkonto) o;
        return Objects.equals(getOid(), bankkonto.getOid()) &&
                Objects.equals(getKontoNummer(), bankkonto.getKontoNummer()) &&
                Objects.equals(getKontoInhaber(), bankkonto.getKontoInhaber()) &&
                Objects.equals(getKontoStand(), bankkonto.getKontoStand()) &&
                Objects.equals(getBuchungen(), bankkonto.getBuchungen());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getKontoNummer(), getKontoInhaber(), getKontoStand(), getBuchungen());
    }

    // Falls man in der toString-Methode auch die Buchungen ausgeben möchte, provoziert dies eine Endlos-Schleife
    // an print Ausgaben, dh. bei jedem toString in einer Buchung wird wieder die toString in Konto aufgerufen,
    // das wieder die toString in den Buchungen aufruft, usw.
    @Override
    public String toString() {
        return "Bankkonto{" +
                "oid=" + oid +
                ", kontoNummer='" + kontoNummer + '\'' +
                ", kontoInhaber='" + kontoInhaber + '\'' +
                ", kontoStand=" + kontoStand +
                '}';
    }
}
