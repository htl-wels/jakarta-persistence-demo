package at.htlwels.it.insy.model.d_onetomany.bidirectional;

import jakarta.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Buchung {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private Date buchungsdatum;
    private Double betrag;
    private String buchungstext;

    @ManyToOne
    @JoinColumn(name = "kontoOID", nullable = false)
    private Bankkonto bankkonto;

    public Buchung() {
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public Date getBuchungsdatum() {
        return buchungsdatum;
    }

    public void setBuchungsdatum(Date buchungsdatum) {
        this.buchungsdatum = buchungsdatum;
    }

    public Double getBetrag() {
        return betrag;
    }

    public void setBetrag(Double betrag) {
        this.betrag = betrag;
    }

    public String getBuchungstext() {
        return buchungstext;
    }

    public void setBuchungstext(String buchungstext) {
        this.buchungstext = buchungstext;
    }

    public Bankkonto getBankkonto() {
        return bankkonto;
    }

    public void setBankkonto(Bankkonto konto) {
        this.bankkonto = konto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buchung)) return false;
        Buchung buchung = (Buchung) o;
        return Objects.equals(getOid(), buchung.getOid()) &&
                Objects.equals(getBuchungsdatum(), buchung.getBuchungsdatum()) &&
                Objects.equals(getBetrag(), buchung.getBetrag()) &&
                Objects.equals(getBuchungstext(), buchung.getBuchungstext()) &&
                Objects.equals(getBankkonto(), buchung.getBankkonto());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getBuchungsdatum(), getBetrag(), getBuchungstext(), getBankkonto());
    }

    @Override
    public String toString() {
        return "Buchung{" +
                "oid=" + oid +
                ", buchungsdatum=" + buchungsdatum +
                ", betrag=" + betrag +
                ", buchungstext='" + buchungstext + '\'' +
                ", konto=" + bankkonto +
                '}';
    }
}
