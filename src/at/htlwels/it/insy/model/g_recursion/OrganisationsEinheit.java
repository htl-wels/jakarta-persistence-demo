package at.htlwels.it.insy.model.g_recursion;

import jakarta.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class OrganisationsEinheit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;

    private String bezeichnung;
    private Integer anzahlMitarbeiter;

    @ManyToOne
    @JoinColumn(name = "UEBERGEORNETEOEOID")
    private OrganisationsEinheit uebergeordneteOrganisationsEinheiten;

    @OneToMany(mappedBy = "uebergeordneteOrganisationsEinheiten", cascade = CascadeType.ALL)
    private List<OrganisationsEinheit> untergeordneteOrganisationsEinheiten;

    public OrganisationsEinheit() {
    }


    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public Integer getAnzahlMitarbeiter() {
        return anzahlMitarbeiter;
    }

    public void setAnzahlMitarbeiter(Integer anzahlMitarbeiter) {
        this.anzahlMitarbeiter = anzahlMitarbeiter;
    }

    public OrganisationsEinheit getUebergeordneteOrganisationsEinheiten() {
        return uebergeordneteOrganisationsEinheiten;
    }

    public void setUebergeordneteOrganisationsEinheiten(OrganisationsEinheit uebergeordneteOrganisationsEinheiten) {
        this.uebergeordneteOrganisationsEinheiten = uebergeordneteOrganisationsEinheiten;
    }

    public List<OrganisationsEinheit> getUntergeordneteOrganisationsEinheiten() {
        return untergeordneteOrganisationsEinheiten;
    }

    public void setUntergeordneteOrganisationsEinheiten(List<OrganisationsEinheit> untergeordneteOrganisationsEinheiten) {
        this.untergeordneteOrganisationsEinheiten = untergeordneteOrganisationsEinheiten;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrganisationsEinheit)) return false;
        OrganisationsEinheit that = (OrganisationsEinheit) o;
        return Objects.equals(getOid(), that.getOid()) &&
                Objects.equals(getBezeichnung(), that.getBezeichnung()) &&
                Objects.equals(getAnzahlMitarbeiter(), that.getAnzahlMitarbeiter()) &&
                Objects.equals(getUebergeordneteOrganisationsEinheiten(), that.getUebergeordneteOrganisationsEinheiten()) &&
                Objects.equals(getUntergeordneteOrganisationsEinheiten(), that.getUntergeordneteOrganisationsEinheiten());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid(), getBezeichnung(), getAnzahlMitarbeiter(), getUebergeordneteOrganisationsEinheiten(), getUntergeordneteOrganisationsEinheiten());
    }


    @Override
    public String toString() {
        return "OrganisationsEinheit{" +
                "oid=" + oid +
                ", bezeichnung='" + bezeichnung + '\'' +
                ", anzahlMitarbeiter=" + anzahlMitarbeiter +
                ", uebergeordneteOrganisationsEinheiten=" + uebergeordneteOrganisationsEinheiten +
                '}';
    }
}