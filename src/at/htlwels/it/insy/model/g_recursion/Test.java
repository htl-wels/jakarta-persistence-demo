package at.htlwels.it.insy.model.g_recursion;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void persistRecursion(String PERSISTENCE_UNIT_NAME) {

        // Recursion
    /*
        ----------------------
        |                    |   1
        |Organisationseinheit|--------
        |                    |       |    uebergeordnet
        ----------------------       |
                   |                 |
                *  |                 |
                   -------------------
                   untergeordnet

         */

        EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager entityManager = factory.createEntityManager();

        System.out.println();
        System.out.println("**************************************************");
        System.out.println("***   Persist an Association - Recursion                  ");

        entityManager.getTransaction().begin();

        OrganisationsEinheit oben = new OrganisationsEinheit();
        oben.setBezeichnung("Oben");
        oben.setAnzahlMitarbeiter(2);
        List<OrganisationsEinheit> listObenUntergeordneteOE = new ArrayList<>();
        oben.setUntergeordneteOrganisationsEinheiten(listObenUntergeordneteOE);

        OrganisationsEinheit mitte = new OrganisationsEinheit();
        mitte.setBezeichnung("Mitte");
        mitte.setAnzahlMitarbeiter(5);
        oben.getUntergeordneteOrganisationsEinheiten().add(mitte);
        List<OrganisationsEinheit> listMitteUntergeordneteOE = new ArrayList<>();
        mitte.setUntergeordneteOrganisationsEinheiten(listMitteUntergeordneteOE);
        mitte.setUebergeordneteOrganisationsEinheiten(oben);

        OrganisationsEinheit unten1 = new OrganisationsEinheit();
        unten1.setBezeichnung("Unten 1");
        unten1.setAnzahlMitarbeiter(20);
        unten1.setUebergeordneteOrganisationsEinheiten(mitte);
        mitte.getUntergeordneteOrganisationsEinheiten().add(unten1);

        OrganisationsEinheit unten2 = new OrganisationsEinheit();
        unten2.setBezeichnung("Unten 2");
        unten2.setAnzahlMitarbeiter(200);
        unten2.setUebergeordneteOrganisationsEinheiten(mitte);
        mitte.getUntergeordneteOrganisationsEinheiten().add(unten2);

        OrganisationsEinheit unten3 = new OrganisationsEinheit();
        unten3.setBezeichnung("Unten 3");
        unten3.setAnzahlMitarbeiter(50);
        unten3.setUebergeordneteOrganisationsEinheiten(mitte);
        mitte.getUntergeordneteOrganisationsEinheiten().add(unten3);

        entityManager.persist(oben);
        entityManager.persist(mitte);

        entityManager.persist(unten1);
        entityManager.persist(unten2);
        entityManager.persist(unten3);

        entityManager.getTransaction().commit();
        entityManager.clear();

        System.out.println("**************************************************");
        System.out.println("***   Validation - Recursion");
        System.out.println("**************************************************");

        // Read Recursion

        entityManager.getTransaction().begin();

        TypedQuery<OrganisationsEinheit> query = entityManager.createQuery("select o from OrganisationsEinheit o", OrganisationsEinheit.class);

        List<OrganisationsEinheit> oeList = query.getResultList();

        // This is pragmatic Code, not more.
        for (OrganisationsEinheit tempOE : oeList) {
            System.out.println("***   " +  tempOE.toString());
        }

        entityManager.getTransaction().commit();

        System.out.println("**************************************************");

        entityManager.close();
    }
}
